 TODO what happens if you use list.remove(Integer.valueOf(5))?
 It removes the first occurence of the element 5
 TODO what happens if you use list.remove(5)?
It removes the sixth element in the list, whereas the list moves from right to left



 TestIterator-TODO also try with a LinkedList - does it make any difference?
 Linked list is better at time complexity in this, as there is remove method here that removes a value from the middle of the list.
 TestList- TODO also try with a LinkedList - does it make any difference?
Arraylist will use up less memory and allow for faster iteration, and it will be faster for insertions that occur at the end of the list.
So Arraylist is better. 







     Reps=1000
     Size                  10      100   1000   10000   100000  1000000
testArrayListAddRemove    26ms    31ms   35ms    27ms    48ms    31ms
testLinkedListAddRemove   24ms    25ms   25ms    27ms    26ms    30ms
testArrayListAccess       20ms    16ms   23ms    22ms    25ms    21ms
testLinkedListAccess      17ms    15ms   16ms    15ms    17ms    17ms